# Keycloak - Alternate DB
Example of how to set an alternate database as a Keycloak Datasource.

Extending the official Dockerfile for Keycloak 6.0.1, this project contains an example of how to set an alternate 
Relational Database - one not supported out of the box. For the purpose of this project, Keycloak is configured to use 
Microsoft SQL Sever Express Edition. 

## Requirements
To build and run this project, was used:
- Maven 3.6.1
- Docker 18.09.2

## How to Build
1) Run `mvn clean package` to build the project.
2) Run `docker compose up` on the root folder of the project. 

Docker Compose will require one environment variable:
- `ACCEPT_EULA`: [Set to Y to indicate that you agree to MS SQL Server License Agreement][MS SQL Server License 
Agreement]

[MS SQL Server License Agreement]:http://go.microsoft.com/fwlink/?LinkId=746388